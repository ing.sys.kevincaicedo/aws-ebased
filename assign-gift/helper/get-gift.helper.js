const SEASONS = {
    SPRING: 'spring',
    SUMMER: 'summer',
    AUTUMN: 'autumn',
    WINTER: 'winter',
};

const GIFTS = {
    [SEASONS.AUTUMN]: 'buzo',
    [SEASONS.WINTER]: 'sweater',
    [SEASONS.SPRING]: 'camisa',
    [SEASONS.SUMMER]: 'remera',
};

const getSeason = (birthDate) => {
    const birth = new Date(birthDate);
    const month = birth.getMonth() + 1;

    if (month >= 3 && month <= 5) return SEASONS.SPRING;
    if (month >= 6 && month <= 8) return SEASONS.SUMMER;
    if (month >= 9 && month <= 11) return SEASONS.AUTUMN;
    return SEASONS.WINTER;
}

const getGift = (birthDate) => {
    return GIFTS[getSeason(birthDate)]
}

module.exports = { getSeason, getGift };
