const { CreatedClientValidation } = require("../schema/input/created-client.input");
const { assignGiftService } = require("../service/gift.service");
const { getGift } = require("../helper/get-gift.helper");

const assignGiftDomain = async (eventPayload, eventMeta) => {
  const payload = JSON.parse(eventPayload.Message);
  new CreatedClientValidation(payload, eventMeta);

  const gift = getGift(payload.birthDate);
  const dni = payload.dni;

  await assignGiftService({ gift, dni });

  return {
    statusCode: 200,
    body: JSON.stringify({ message: 'Gift assigned successfully' }),
  };
};

module.exports = { assignGiftDomain };
