const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class GiftAssigned extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'GIFT.ASSIGN_GIFT',
      specversion: 'v1.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        gift: { type: String, required: true },
        dni: { type: String, required: true }
      },
    })
  }
}

module.exports = { GiftAssigned };
