const { batchEventMapper } = require("ebased/handler");
const inputMode = require('ebased/handler/input/batchEventQueue');
const outputMode = require('ebased/handler/output/batchEventConfirmation');

const { assignGiftDomain } = require("../domain/assign-gift.domain");

const handler = async (events, context) => {
  return batchEventMapper(
    { events, context },
    inputMode,
    assignGiftDomain,
    outputMode
  );
};

module.exports = { handler };
