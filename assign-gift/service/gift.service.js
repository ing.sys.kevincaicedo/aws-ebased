const dynamo = require('ebased/service/storage/dynamo');

const assignGiftService = async (payload) => {
  await dynamo.updateItem({
    TableName: process.env.CLIENTS_TABLE,
    Key: {
      dni: payload.dni
    },
    UpdateExpression: 'set gift = :g',
    ExpressionAttributeValues: {
      ':g': { S: payload.gift }
    }
  });
}

module.exports = { assignGiftService };
