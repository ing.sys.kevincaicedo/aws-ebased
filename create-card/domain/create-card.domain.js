const { CreatedClientValidation } = require("../schema/input/created-client.input");
const { createCardService } = require("../service/card.service");
const { generateCard } = require("../helper/generate-card.helper");

const createCardDomain = async (eventPayload, eventMeta) => {
  const payload = JSON.parse(eventPayload.Message);
  new CreatedClientValidation(payload, eventMeta);

  const creditCard = generateCard(payload.birthDate);
  const dni = payload.dni;

  await createCardService({ ...creditCard, dni });

  return {
    statusCode: 200,
    body: JSON.stringify({ message: 'Card created successfully' }),
  };
};

module.exports = { createCardDomain };