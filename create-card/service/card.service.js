const dynamo = require('ebased/service/storage/dynamo');

const createCardService = async (payload) => {
  await dynamo.putItem({
    TableName: process.env.CARDS_TABLE,
    Item: payload
  });
}

module.exports = { createCardService };
