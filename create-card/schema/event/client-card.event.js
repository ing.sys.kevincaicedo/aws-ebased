const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class CardCreated extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      type: 'CARD.CREATE_CARD',
      specversion: 'v1.0',
      payload: payload,
      meta: meta,
      schema: {
        strict: false,
        cardNumber: { type: String, required: true },
        expirationMonth: { type: String, required: true },
        expirationYear: { type: String, required: true },
        securityCode: { type: String, required: true }
      },
    })
  }
}

module.exports = { CardCreated };
