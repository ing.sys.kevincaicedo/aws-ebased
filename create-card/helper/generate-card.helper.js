const { calculateAge } = require("./calculate-age.helper");

const generateCard = (birthDate) => {
    const today = new Date();
    const birth = new Date(birthDate);
    // determine card type based on age (Classic for under 45, Gold for over)
    const age = calculateAge(birth);
    let cardType = 'Classic';
    if (age >= 45) {
        cardType = 'Gold';
    }

    // generate card number, expiration date, and security code
    const cardNumber = Math.floor(1000000000000000 + Math.random() * 9000000000000000).toString();
    const expirationMonth = (Math.floor(Math.random() * 12) + 1).toString(); // random number between 1 and 12
    const expirationYear = (today.getFullYear() + Math.floor(Math.random() * 5) + 1).toString(); // random number between 1 and 5 years from now
    const securityCode = Math.floor(100 + Math.random() * 900).toString();

    return {
        cardType,
        cardNumber,
        expirationMonth,
        expirationYear,
        securityCode,
    };
}

module.exports = { generateCard };
