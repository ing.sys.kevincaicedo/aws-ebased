const { CreateClientValidation } = require("../schema/input/create-client.input");
const { createClientService } = require("../service/client.service");
const { publishClientCreated } = require("../service/publish.service");
const { ClientCreated } = require("../schema/event/client-created.event");
const { calculateAge } = require("../helper/calculate-age.helper");

const createClientDomain = async (payload, commandMeta) => {
  new CreateClientValidation(payload, commandMeta);

  const birthDate = new Date(payload.birthDate);
  const age = calculateAge(birthDate);
  if (age < 18 || age > 65) {
    return {
      statusCode: 400,
      body: JSON.stringify({ message: "Client must be between 18 and 65 years old" }),
    };
  }

  await createClientService(payload);
  await publishClientCreated(new ClientCreated(payload, commandMeta))

  return {
    statusCode: 200,
    body: JSON.stringify({ message: 'Client created successfully' }),
  };
};

module.exports = { createClientDomain };