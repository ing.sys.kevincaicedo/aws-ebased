const sns = require("ebased/service/downstream/sns");

const publishClientCreated = async (event) => {
  const { eventPayload, eventMeta } = event.get();

  await sns.publish({
    TopicArn: process.env.CLIENTS_CREATED_TOPIC,
    Message: eventPayload,
  }, eventMeta);
}

module.exports = { publishClientCreated };
