const dynamo = require('ebased/service/storage/dynamo');

const createClientService = async (payload) => {
  await dynamo.putItem({
    TableName: process.env.CLIENTS_TABLE,
    Item: payload
  });
}

module.exports = { createClientService };
